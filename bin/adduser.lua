local tArgs = {...}
if #tArgs < 1 or #tArgs > 2 then
  tweaks.setColor(colors.yellow, colors.black)
  print("Usage: <user> [<home dir>]")
else
  user.newUser(unpack(tArgs))
end