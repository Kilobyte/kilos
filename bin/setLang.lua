local tArgs = {...}
if #tArgs ~= 1 then
  print("Usage: setLang <language>")
else
  if lang.load(tArgs[1]) then
    user.setLang(tArgs[1])
  else
    print("Invalid language")
  end
end