--local url = "http://stiepen.bplaced.net/KilOS/"
--local url = "http://localhost/KilOSupdater/"

local tArgs = {...}

local debug, dev, source, noupdate, sim, git
local tArgs_ = {}


for a = 1, #tArgs do
  if tArgs[a]:sub(1,2) == "--" then
    local cmd = tArgs[a]:sub(3):lower()
    if cmd == "debug" then
      debug = true
    elseif cmd == "help" then
      man.open("update")
      noupdate = true
    elseif cmd == "source" then
      source = tArgs[a+1]
      if source:sub(-1, -1) ~= "/" then
        source = source.."/"
      end
      a = a + 1
    elseif cmd == "dev" then 
      dev = true
    elseif cmd == "simulate" then
      sim = true
    elseif cmd == "git" then
      git = true
    end
  elseif tArgs[a]:sub(1,1) == "-" then
    for b = 2, #tArgs[a] do
      cmd = tArgs[a]:sub(b, b)
      if cmd == "h" then
 	     man.open("update")
 	     noupdate = true
      elseif cmd == "d" then
        dev = true
      elseif cmd == "s" then
        sim = true
      elseif cmd == "g" then
        git = true
      end
    end
  else
    table.insert(tArgs_, tArgs[a])
  end
end

local url = source or KilOS.getUpdateUrl(dev)

--[[local function createDirR(dir)
  print("creating dir "..dir)
  if dir == "/" or not dir then return end
  if fs.exists(dir) then
    return
  end
  if dir:sub(-1, -1) == "/" then dir = dir:sub(1, -2) end
  createDirR(dir:sub(1, #fs.getName(dir) + 3))
  fs.makeDir(dir)
end--]]

function main()
if http then
  if sim then print("Simulating...") end
  print("Downloading Filelist...")
  local list = url.."list.php?osver="..textutils.urlEncode(KilOS.getVersion()).."&dispver=1"
  if debug then print("File list: "..list) end
  local h = http.get(list)
  local files = {}
  if not h or h.getResponseCode() ~= 200 then 
    printError("Somehow the file list couldn't be retrieved")
    return
  end
  while true do
    local l = h.readLine()
    if not l then break end
    table.insert(files, l)
  end
  h.close()
  print()
  print("Done. "..#files.." files to download")
  local x, y = term.getCursorPos()
  
  local nscroll = term.scroll
  local function scroll_(n)
    y = y - n
    nscroll(n)
  end
  rawset(term, "scroll", scroll_)
  local s, m = pcall(function()
    for a = 1, #files do
      term.setCursorPos(x, y)
      write("Downloading File "..a.." of "..#files.."...")
      h = http.get(url.."files/"..files[a])
      local dir = files[a]:sub(1, (#fs.getName(files[a]) + 2) * (-1))
      if not (dir == "" or dir == "/" or fs.exists(dir)) then
        fs.makeDir(dir)
      end
      if not sim then
        local f = fs.open(files[a], "w") 
        if not f then error("Couldn't open target file.") end
        if not h then error("Couldn't download file.") end
        f.write(h.readAll())
        f.close()
      end
      h.close()
    end
  end)
  rawset(term, "scroll", nscroll)
  print()
  if s then
    if not sim then 
      print("Done, Press any key to reboot")
      os.pullEvent("key")
      os.reboot()
    else
      print("Done.")
    end
  else
    printError("Update failed due to internal error: "..m)
  end
else
  printError("HTTP API needs to be enabled for this to work")
end
end
if git and not noupdate then
  shell.run("/usr/bin/devupdate.lua")
  noupdate = true
end
local trash = (noupdate or main())