os.run(getfenv(), "/lib/winmgr/window.lua")
local windows = {}

local w, h = term.getSize()

window.init(0, 2, w, h - 2)

local function runInWin(file, ...)
  local win = window.new()
  local env = {win = win, term = window.getTerm(win)}
  window.focus(win)
  return os.run(env, file, ...)
end

local tArgs = {...}

return runInWin(tArgs[1])