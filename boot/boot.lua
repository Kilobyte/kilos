--[[
  This is the main boot code.
  
  purpose:
  * load the kernel
  * catch and report kernel errors
--]]
rawset(_G, "_loadfile", loadfile)
loadfile = function(file, name)
  if name then
    local f = fs.open(file, "r")
    return loadstring(f.readAll(), name)
  else
    return _loadfile(file)
  end
end

rawset(_G, "loadfile", loadfile)

local function conc(first, ...)
  if first then
    return tostring(first)..tostring(conc(...))
  end
  return ""
end

--[[fs.open("print.log", "w").close()
local p1 = print
local function p(...)
  local tArgs = {...}
  p1(unpack(tArgs))
  local s = conc(unpack(tArgs))
  local f = fs.open("print.log", "a")
  f.writeLine(s)
  f.close()
end
rawset(_G, "print", p)--]]

local s, m = loadfile("/sbin/kernel.lua", "Kernel")

if not s then error("Could not load Kernel: "..m) end

local s1, m = pcall(s)

if not s1 then
  printError(m)
end

shell.exit()

sleep(5)