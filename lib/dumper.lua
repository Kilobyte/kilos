function dump(t, tr, pre)
  t = t or _G
  tr = tr or {}
  pre = (pre or "")
  local tmp = ""
  for k, v in pairs(t) do
    if type(v) == "table" then
      if tr[v] then
        tmp = tmp..pre..tostring(k).." = [Pointer to "..tostring(v).."]\n"
      else
        tr[v] = true
        tmp = tmp..pre..tostring(k).." = "..tostring(v).." {\n"..dump(v, tr, pre.."  ")..pre.."}\n"
      end
    else
      tmp = tmp..pre..tostring(k).." = "
      local tv = type(v)
      if tv == "string" then
        tmp = tmp.."\""..v.."\"\n"
      else
        tmp = tmp..tostring(v).."\n"
      end
    end
  end
  return tmp
end