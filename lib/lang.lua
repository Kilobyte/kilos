--language system

local strings = {}
local p = print
local w = write

function load(name)
  if fs.exists("/lang/"..name..".lang") then
    s, m =  pcall(function()
      strings = config.load("/lang/"..name..".lang")
    end)
    if not s then printError(m) end
    return s, m
  else
    --printError("failed")
    return false
  end
end

function get(...)
  local args = {...}
  local s = strings
  
  for a = 1, #args do
    if s[args[a]] then
      s = s[args[a]]
    end
  end
  return s
end

function getRepl(str, repl)
  local tmp = get(unpack(str))
  for k, v in pairs(repl) do
    tmp = tmp:gsub("%[%[%$"..k.."%]%]", v)
  end
  return tmp
end

function print(...)
  --p("getting ", ...)
  p(get(...))
end

function write(...)
  w(get(...))
end