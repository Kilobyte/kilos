--[[
  Author of this file is max96at. a big thanks to him.
  (note added by Kilobyte)
--]]

local function listToTable(sPathList)
  local tPathList = {}
  for str in sPathList:gmatch('[^:]+') do table.insert(tPathList, str) end
  return tPathList
end
 
function add(sPathList,sPath,pos)
  local tPathList = listToTable(sPathList)
  if pos then table.insert(tPathList,pos,sPath)
  else table.insert(tPathList,sPath) end
  return table.concat(tPathList,":")
end
 
function remove(sPathList,sPath)
  local tPathList = listToTable(sPathList)
  local nPathToRemove = 0
  for i=1, #tPathList do
    if tPathList[i] == sPath then nPathToRemove = i break end
  end
  if nPathToRemove > 0 then
    table.remove(tPathList,nPathToRemove)
  end
  return table.concat(tPathList,":")
end