--[[
  special thanks to Nia who fixed all the security holes (i don't have the knowlage :P)
--]]

local COREAPIS = {"redstone", "peripheral", "bit", "coroutine", "math", "fs", "os", "string", "table",
"type", "setfenv", "loadstring", "pairs", "rawequal", "ipairs", "xpcall",
"read", "unpack", "setmetatable", "rawset", "rs", "http", "rawget", "printError",
"sleep", "assert", "error", "tostring", "tonumber", "loadfile", "write", "print",
"select", "term", "pcall", "turtle", "rednet", 
 
"stringutils", "table_", "tweaks", "lang",
}
 
local function protect(x, proxied)
  if not proxied then
    proxied = {}
  end
 
  if proxied[x] then
    return proxied[x]
  end
 
  if type(x) == "table" then
    local n = {}
    proxied[x] = n
    for k, v in pairs(x) do
      n[k] = protect(v, proxied)
    end
    return n
  else
    return x
  end
end
 
function makeSandbox(_user)
  local n_G = _G
  if not _user then _user = user end
  local s = {}
  for a = 1, #COREAPIS do
    s[COREAPIS[a]] = _G[COREAPIS[a]]
  end
  s.os = {
    version = KilOS.version,
    computerID = os.computerID,
    getComputerID = os.getComputerID,
    getComputerLabel = os.getComputerLabel,
    setComputerLabel = function(label)
      if _user.getName() == "root" then
        os.setComputerLabel(label)
        return true
      else
        return false, "You are no root"
      end
    end,
    pullEvent = os.pullEvent,
    pullEventRaw = os.pullEventRaw,
    queueEvent = os.queueEvent,
    clock = os.clock,
    startTimer = os.startTimer,
    time = os.time,
    setAlarm = os.setAlarm,
    shutdown = os.shutdown,
    reboot = os.reboot
  }
 
  s.KilOS = {
    getVersion = KilOS.getVersion,
    getUpdateUrl = KilOS.getUpdateUrl
  }
  
  s.next = function(tab, ind)
    if type(tab) ~= "table" then
      error("Parameter #1: table expected, got "..type(tab))
    end
    local mt = getmetatable(tab)
    if mt then
      if mt.__next and type(mt.__next) == "function" then
        return mt.__next(tab, ind)
      end
    end
    return next(tab, ind)
  end
 
  local safefenv = setmetatable({}, {__mode = "k"})
  local fakefenv = setmetatable({}, {__mode = "k"})
 
  local function getFunction(level)
    if type(level) == "number" and level > 0 then
      level = level + 1
    end
    return setfenv(level, getfenv(level))
  end
  s.setfenv = function(level, env)
    if type(level) == "number" and level > 0 then
      level = level + 1
    end
    local oldenv = getfenv(level)
    if not safefenv[oldenv] then
      local fn = getFunction(level)
      fakefenv[fn] = env
      return fn
    else
      safefenv[env] = true
      return setfenv(level, env)
    end
  end
  s.getfenv = function(level)
    if not level then
      level = 1
    end
    if type(level) == "number" and level > 0 then
      level = level + 1
    end
    local env = getfenv(level)
    if not safefenv[env] then
      local fn = getFunction(level)
      if fakefenv[fn] then
        return fakefenv[fn]
      else
        return s
      end
    else
      return env
    end
  end
 
  s.loadstring = function(...)
    local fn, err = loadstring(...)
    if fn then
      setfenv(fn, s)
    end
    return fn, err
  end
  s.loadfile = function(...)
    local fn, err = loadfile(...)
    if fn then
      setfenv(fn, s)
    end
    return fn, err
  end
  s.dofile = function(file)
    local fn, err = loadfile(file)
    if fn then
      setfenv(fn, s)
      return fn()
    else
      error(err)
    end
  end
 
  s.user = _user
 
  s._G = s
 
  local protectT = {} -- preserve reference equality
  for k,v in pairs(s) do
    s[k] = protect(v, protectT)
  end
  safefenv[s] = true
 
  return s
end