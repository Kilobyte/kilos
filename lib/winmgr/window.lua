window = {}
window.__index = window
local allwins = {}
local focussed = nil
local xOff, yOff, height, width, redrawcallback

function window.init(x, y, w, h, r)
  xOff, yOff, height, width, redrawcallback = x, y, h, w, r
end

function window.new()
  local wnd = {}
  setmetatable(wnd, window)
  wnd = {
    id = tonumber(tostring(math.random()):sub(3)),
    title = "",
    lines = {},
    blink = false,
    x = 1,
    y = 1,
    textcolor = colors.white,
    backcolor = colors.black
  }
  for a = 1, height do
    table.insert(wnd.lines, (" "):rep(width))
  end
  while allwins[wnd.id] do
    wnd.id = tonumber(tostring(math.random()):sub(3))
  end
  allwins[wnd.id] = wnd
  return wnd
end

function window:termcall(func, ...)
  local tArgs = {...}
  --if self:focussed() and term[func] then term[func](...) end
  if func == "clear" then
    for a = 1, height do
      table.insert(self.lines, (" "):rep(width))
    end
  elseif func == "write" then
    if type(tArgs[1]) ~= "string" then
      error("Parameter #1: string expected, got "..type(tArgs[1]))
    end
    local text = tArgs[1]
    local x
    if #text + self.x > width then
      text = text:sub(width - self.x)
      x = width
    elseif #text + self.x < width then
      x = self.x + #text
      text = text..self.lines[self.y]:sub(width - self.x - #text + 1)
    else
      x = width
    end
    self.lines[self.y] = self.lines[self.y]:sub(1, self.x - 1)..text
    self.x = x
  elseif func == "clearLine" then
    self.lines[self.y] = (" "):rep(width)
  elseif func == "getCursorPos" then
    return self.x, self.y
  elseif func == "setCursorPos" then
    if type(tArgs[1]) ~= "number" then
      error("Parameter #1: number expected, got "..type(tArgs[1]))
    end
    if type(tArgs[2]) ~= "number" then
      error("Parameter #2: number expected, got "..type(tArgs[2]))
    end
    if tArgs[1] > width or tArgs[1] < 1 then
      error("Param #1 out of range")
    end
    if tArgs[2] > height or tArgs[2] < 1 then
      error("Param #2 out of range")
    end
    self.x, self.y = tArgs[1], tArgs[2]
  elseif func == "setBlink" then
    self.blink = (Args[1] == true)
  elseif func == "isColor" or func == "isColour" then
    return term.isColor()
  elseif func == "getSize" then
    return width, height
  elseif func == "scroll" then
    if type(tArgs[1]) ~= "number" then
      error("Parameter #1: number expected, got "..type(tArgs[1]))
    end
    if tArgs[1] == 0 then
      return
    elseif tArgs[1] > 0 then
      for a = 1, tArgs[1] do
        table.remove(self.lines, 1)
        self.lines[#self.lines + 1] = (" "):rep(width)
      end
    else
      for a = 1, (tArgs[1] * (-1)) do
        table.remove(self.lines, #self.lines)
        table.insert(1, self.lines, (" "):rep(width))
      end
    end
  elseif func == "redirect" then
  
  elseif func == "restore" then
  
  elseif func == "setTextColor" or func == "setTextColour" then
    textcolor = tArgs[1]
  elseif func == "setBackgroundColor" then
    
  end
  window.redraw(self)
end

function window:focus()
  focussed = self.id
end

function window:redraw()
  if window.focussed(self) then
    term.clear()
    if redrawcallback then redrawcallback(self) end
    for a = 1, #self.lines do
      term.setCursorPos(1 + xOff, a + yOff)
      term.write(self.lines[a])
    end
  end
end

function window:destroy()

end

function window:focussed()
  return focussed == self.id
end

function window:getTerm()
  local t = {}
  local tf = {"write", "clear", "clearLine", "getCursorPos", "setCursorPos", "setCursorBlink", "isColor", "getSize", "scroll", "redirect", "restore", "setTextColor", "setBackgroundColor"}
  for k, v in ipairs(tf) do
    t[v] = function(...)
      --print(self, v)
      return window.termcall(self, v, ...)
    end
  end
  return t
end

rawset(_G, "window", window)