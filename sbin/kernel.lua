--[[
  The core os file
  
--]]

-- some defines. these are not needed to change usually

local VERSION = "Pre-Alpha 2.3.1"
local COREAPIS = {"redstone", "peripheral", "bit", "coroutine", "math", "fs", "os", "string", "table"}
local updateurl = "http://stiepen.bplaced.net/KilOS/"
local devupdate = "http://stiepen.bplaced.net/KilOSdev/"
--local updateurl = "http://localhost/KilOSupdater/" --Was for Testing :P

-- copying core apis

for k = 1, #COREAPIS do
  local v = COREAPIS[k]
  rawset(_G, "_"..v, _G[v])
end

-- modifying some core apis


-- core functions

local function getVersion()
  return VERSION
end

local function getUpdateUrl(dev)
  if dev then
    return devupdateurl
  end
  return updateurl
end
local f = fs.open("/KilOS.log", "w").close()
local function log(text, level, p)
  local f = fs.open("/KilOS.log", "a")
  local l = "INFO"
  local c = 1
  if level == 1 then
    l = "WARNING"
    c = 2
  elseif level == 3 then
    l = "ERROR"
    c = 16384
  elseif level == 4 then
    l = "CRITICAL"
    c = 16384
  end
  f.writeLine("["..l.."] "..tostring(text))
  f.close()
  if p then 
    if tweaks then tweaks.setColor(c) end
    print("["..l.."] "..tostring(text))
    if tweaks then tweaks.setColor(1) end
  end
end

rawset(_G, "KilOS", {getVersion = getVersion, getUpdateUrl = getUpdateUrl, log = log})

function getAPI(file)
  if fs.isDir(file) then return end
  local tApi = {}
  setmetatable(tApi, {__index = _G})
  os.run(tApi, file)
  return tApi
end

-- loading internal apis

local apis = fs.list("/lib/")
for k = 1, #apis do
  local fname = "/lib/"..apis[k]
  local name = fs.getName(fname)
  if fname:sub(-4, -1) == ".lua" and #fname > 4 then
    name = name:sub(1, -5)
  end
  --log("loading API "..fname.." as "..name)
  local api = getAPI(fname)
  if api then rawset(_G, name, api) end
end

-- loading Kernel Modules

if fs.exists("/sbin/mod/") then
local mods = fs.list("/sbin/mod/")
for i = 1, #mods do
  local s, m = pcall(function()
    loadfile("/sbin/mod/"..mods[i])
    --log("loading Kernel module"..mods[i])
  end)
  if not s then
    error("Failed to load Kernel module "..mods[i]..". error: "..m)
  end
end
end

function dump_G()
  local d = dumper.dump(_G)
  local f = fs.open("_G.dump", "w")
  f.write(d)
  f.close()
end

lang.load("en-us")

--check for Updates
if http then
  tweaks.setColor(colors.blue)
  --print("Checking for updates...")
  lang.print("update", "check")
  local x, y = term.getCursorPos()
  local step = 1
  local h = http.get(updateurl.."version.txt")
  local ver
  if h then ver = h.readLine() end
  if ver == VERSION then
    tweaks.setColor(colors.green)
    lang.print("update", "ok")
  elseif ver then
    tweaks.setColor(colors.red)
    print(lang.get("update", "new1")..ver)
    print(lang.get("update", "new2")..h.readAll())
    print(lang.get("update", "new3"))
  else
    tweaks.setColor(colors.red)
    lang.print("update", "err_nover")
  end
  h.close()
else
  tweaks.setColor(colors.orange)
  lang.print("update", "err_nohttp")
end

user.init()
user.lock(true)

-- loading shell
sbx = sandbox.makeSandbox(user)
local f, m = loadfile("/sbin/sandboxstarter.lua")
setfenv(f, sbx)
if f then 
  local s, m = pcall(f)
  if not s then 
    printError(m)
    dump_G() 
  end
else 
  printError(m) 
  dump_G() 
end