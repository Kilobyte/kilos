local per = peripheral
local p = {}
local sides = {"left", "right", "top", "bottom", "front", "back"}
local colors = { "green", "brown", "black", "pink", "yellow", "orange", "magenta", "purple", "cyan", "red", "white", "lightBlue", "lightGray", "gray", "lime", "blue" }

p.search = function(name)
  ret = {}
  for a = 1, #sides do
    local v = sides[a]
    if peripheral.getName(v) == name then
      table.insert(ret, v)
    end
    if peripheral.getName(v) == "cable" then
      for b = 1, #colors do
        v2 = v..":"..colors[b]
        if peripheral.getName(v2) == name then
          table.insert(ret, v2)
        end
      end
    end
  end
  return unpack(ret)
end

rawset(peripheral, "search", p.search)
log("loaded peripheral yayness")