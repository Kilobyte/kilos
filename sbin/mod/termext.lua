rawset(term, "setColor", tweaks.setColor)
rawset(term, "setColour", tweaks.setColor)
local tc, bc = colors.white, colors.black
local getTextColor = function()
  return tc
end
local getBackgroundColor = function()
  return bc
end
rawset(term, "getTextColor", getTextColor)
rawset(term, "getTextColour", getTextColor)
rawset(term, "getBackgroundColor", getBackgroundColor)
rawset(term, "getBackgroundColour", getBackgroundColor)
local setTextColor = term.setTextColor
local setBackgroundColor = term.setBackgroundColor
term.setTextColor = function(c)
  --KilOS.log("Set Text color to "..tostring(c))
  --KilOS.log(type(KilOS.log)..type(getfenv().KilOS.log))
  setTextColor(c)
  if c == colors.black or c == colors.white or term.isColor() then
    tc = c
  end
end
term.setTextColour = term.setTextColor
term.setBackgroundColor = function(c)
  --KilOS.log("Set Background color to "..tostring(c))
  n_G.term.setBackgroundColor(c)
  if c == colors.black or c == colors.white or term.isColor() then
    bc = c
  end
end
term.setBackgroundColour = s.term.setBackgroundColor