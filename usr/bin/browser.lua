--[[
		file selection and dilog box API / lib / other
		by BigSHinyToys
		note: send link to nightin9ale on CC forums
--]]

local tArgs = {...}
local ver = "0.6_a"
local bugTest, norun, dir, showAll
local _tArgs = {}

for a = 1, #tArgs do
  if tArgs[a]:sub(1,2) == "--" then
    local cmd = tArgs[a]:sub(3):lower()
    if cmd == "debug" then
      bugTest = true
    elseif cmd == "help" then
      man.open("browser")
      norun = true
    elseif cmd == "dir" then
      dir = tArgs[a+1]
      a = a + 1
    elseif cmd == "all" then
      showAll = true
    end
  elseif tArgs[a]:sub(1,1) == "-" then
    for b = 2, #tArgs[a] do
      cmd = tArgs[a]:sub(b, b)
      if cmd == "d" then
        bugTest = true
      elseif cmd == "h" then
        man.open("browser")
        norun = true
      elseif cmd == "p" then
        dir = tArgs[a+1]
        a = a + 1
      elseif cmd == "a" then
        showAll = true
      end
    end
  else
    table.insert(_tArgs, tArgs[a])
  end
end

if (not dir) and shell and shell.dir then
  dir = shell.dir()
end

if dir and shell and shell.resolve then
  dir = shell.resolve(dir)
end

dir = dir or "/"

if bugTest then -- this is that the var is for testing
	print("Dir: "..dir)
	sleep(4)
end

local function clear()
	term.setBackgroundColor(colors.black)
	term.setTextColor(colors.white)
	term.clear()
	term.setCursorBlink(false)
	term.setCursorPos(1,1)
end

--[[
		Code thanks to Cruor 
		http://www.computercraft.info/forums2/index.php?/topic/5802-support-for-shell/
--]]
local function fixArgs(...)
	local tReturn={}
	local str=table.concat({...}," ")
	local sMatch
	while str and #str>0 do
		if string.sub(str,1,1)=="\"" then
			sMatch, str=string.match(str, "\"(.-)\"%s*(.*)")
		else
			sMatch, str=string.match(str, "(%S+)%s*(.*)")
		end
		table.insert(tReturn,sMatch)
	end
	return tReturn
end
--[[ end Cruor function --]]


-- modified read made to play nice with coroutines

local function readMOD( _sReplaceChar, _tHistory,_wdth)
	local sLine = ""
	term.setCursorBlink( true )

	local nHistoryPos = nil
	local nPos = 0
    if _sReplaceChar then
		_sReplaceChar = string.sub( _sReplaceChar, 1, 1 )
	end
	
	local sx, sy = term.getCursorPos()	

	local w, h = term.getSize()
	if _wdth and type(_wdth) == "number" then
		w = sx + _wdth - 1
	end
	
	local function redraw( _sCustomReplaceChar )
		local nScroll = 0
		if sx + nPos >= w then
			nScroll = (sx + nPos) - w
		end
			
		term.setCursorPos( sx + _wdth - 1, sy )
		term.write(" ")
		term.setCursorPos( sx, sy )
		local sReplace = _sCustomReplaceChar or _sReplaceChar
		if sReplace then
			term.write( string.rep(sReplace,_wdth) )
		else
			term.write( string.sub( sLine, nScroll + 1 ,nScroll + _wdth) )
		end
		term.setCursorPos( sx + nPos - nScroll, sy )
	end
	
	while true do
		local sEvent, param = os.pullEvent()
		if sEvent == "char" then
			sLine = string.sub( sLine, 1, nPos ) .. param .. string.sub( sLine, nPos + 1 )
			nPos = nPos + 1
			redraw()
			
		elseif sEvent == "key" then
			
			if param == keys.left then
				-- Left
				if nPos > 0 then
					nPos = nPos - 1
					redraw()
				end
				
			elseif param == keys.right then
				-- Right				
				if nPos < string.len(sLine) then
					nPos = nPos + 1
					redraw()
				end
			
			elseif param == keys.up or param == keys.down then
                -- Up or down
				if _tHistory then
					redraw(" ");
					if param == keys.up then
						-- Up
						if nHistoryPos == nil then
							if #_tHistory > 0 then
								nHistoryPos = #_tHistory
							end
						elseif nHistoryPos > 1 then
							nHistoryPos = nHistoryPos - 1
						end
					else
						-- Down
						if nHistoryPos == #_tHistory then
							nHistoryPos = nil
						elseif nHistoryPos ~= nil then
							nHistoryPos = nHistoryPos + 1
						end						
					end
					
					if nHistoryPos then
                    	sLine = _tHistory[nHistoryPos]
                    	nPos = string.len( sLine ) 
                    else
						sLine = ""
						nPos = 0
					end
					redraw()
                end
			elseif param == keys.backspace then
				-- Backspace
				if nPos > 0 then
					redraw(" ");
					sLine = string.sub( sLine, 1, nPos - 1 ) .. string.sub( sLine, nPos + 1 )
					nPos = nPos - 1					
					redraw()
				end
			elseif param == keys.home then
				-- Home
				nPos = 0
				redraw()		
			elseif param == keys.delete then
				if nPos < string.len(sLine) then
					redraw(" ");
					sLine = string.sub( sLine, 1, nPos ) .. string.sub( sLine, nPos + 2 )				
					redraw()
				end
			elseif param == keys["end"] then
				-- End
				nPos = string.len(sLine)
				redraw()
			end
		elseif sEvent == "redraw" then
			redraw()
		elseif sEvent == "return" then
			term.setCursorBlink( false )
			return sLine
		end
	end
	
	term.setCursorBlink( false )
	
	return sLine
end

-- end modified read

local function printC(posX,posY,textCol,backCol,text)
	term.setCursorPos(posX,posY)
	term.setTextColor(textCol)
	term.setBackgroundColor(backCol)
	term.write(text)
end

local function InputBox(title)
	local boxW,boxH = 26,3
	local termX,termY = term.getSize()
	local ofsX,ofsY = math.ceil((termX/2) - (boxW/2)) , math.ceil((termY/2) - (boxH/2)) -- offset from top left
	local options = {"ok","cancel"}
	
	local selected = 1
	local space = 0
	local range = {}
	for i = 1,#options do
		range[i] = {s = space,f = space + string.len(options[i])}
		space = space + string.len(options[i])+3
	end
	local ofC = (boxW/2) - (space/2)
	
	local function drawBox()
		printC(ofsX,ofsY,colors.black,colors.blue,string.rep(" ",boxW))
		printC(ofsX+1,ofsY,colors.black,colors.blue,(title or "User Input"))
		printC(ofsX,ofsY+1,colors.black,colors.white,string.rep(" ",boxW))
		printC(ofsX,ofsY+2,colors.black,colors.white,string.rep(" ",boxW))
		printC(ofsX,ofsY+3,colors.black,colors.white,string.rep(" ",boxW))
		
		for i = 1,#options do
			if i == selected then
				term.setBackgroundColor(colors.lightGray)
				term.setCursorPos(range[i].s + ofC + ofsX - 1,ofsY + 3)
				term.write("["..options[i].."]")
				term.setBackgroundColor(colors.white)
				term.write(" ")
			else
				term.setCursorPos(range[i].s + ofC + ofsX - 1,ofsY + 3)
				term.write(" "..options[i].." ")
			end
		end
		
		printC(ofsX+2,ofsY+2,colors.black,colors.lightGray,string.rep(" ",boxW-4))
	end
	drawBox()
	term.setCursorPos(ofsX+2,ofsY+2)
	local co = coroutine.create(function() return readMOD(nil,nil,boxW - 4) end)
	while true do
		local event = {os.pullEvent()}
		if event[1] == "key" or event[1] == "char" then
			if event[2] == 28 then
				local test,data = coroutine.resume(co,"return")
				return data
			else
				coroutine.resume(co,unpack(event))
			end
		elseif event[1] == "mouse_click" then
			if event[4] == ofsY + 3 then
				for i = 1,#options do
					if event[3] >= range[i].s + ofC + ofsX - 1 and event[3] <= range[i].f + ofC + ofsX then
						if options[i] == "ok" then
							local test,data = coroutine.resume(co,"return")
							return data
						elseif options[i] == "cancel" then
							return
						end
					end
				end
			end
		end
	end
end

local function dialogBox(title,message,options, h, w)
	term.setCursorBlink(false)
	local selected = 1
	title = title or ""
	message = message or ""
	options = options or {}
	local boxW,boxH = (w or 26), (h or 3)
	local termX,termY = term.getSize()
	local ofsX,ofsY = math.ceil((termX/2) - (boxW/2)) , math.ceil((termY/2) - (boxH/2)) -- offset from top left
	
	local space = 0
	local range = {}
	for i = 1,#options do
		range[i] = {s = space,f = space + string.len(options[i])}
		space = space + string.len(options[i])+3
	end
	local ofC = math.ceil((boxW/2)) - math.ceil((space/2))
	
	local function drawBox()
		if term.isColor() then
			term.setTextColor(colors.black)
			term.setCursorPos(ofsX,ofsY) -- F*** YOU 3 hours to find out i forgot to round numbers before sending them to this Fing function
			term.setBackgroundColor(colors.blue)
			term.write(" "..title..string.rep(" ",boxW-#title-5).."_[]")
			term.setBackgroundColor(colors.red)
			term.setTextColor(colors.white)
			term.write("X")
			term.setCursorPos(ofsX,ofsY+1)
			term.setBackgroundColor(colors.white)
			term.setTextColor(colors.black)
			term.write(string.sub(" "..message..string.rep(" ",boxW),1,boxW)) -- edited
			term.setCursorPos(ofsX,ofsY+2)
			term.write(string.rep(" ",boxW))
			term.setCursorPos(ofsX,ofsY+3)
			term.write(string.rep(" ",boxW))
			
			for i = 1,#options do
				if i == selected then
					term.setBackgroundColor(colors.lightGray)
					term.setCursorPos(range[i].s + ofC + ofsX - 1,ofsY + 3)
					term.write("["..options[i].."]")
					term.setBackgroundColor(colors.white)
					term.write(" ")
				else
					term.setCursorPos(range[i].s + ofC + ofsX - 1,ofsY + 3)
					term.write(" "..options[i].." ")
				end
			end
			term.setCursorPos(ofsX + ofC + space,ofsY + 3)
			term.write(string.rep(" ",boxW - (ofC + space)))
			term.setBackgroundColor(colors.black)
			term.setTextColor(colors.white)			
		end
	end
	while true do
		drawBox()
		event = {os.pullEvent()}
		if event[1] == "key" then
			if event[2] == 203 then -- left
				selected = selected - 1
				if selected < 1 then
					selected = #options
				end
			elseif event[2] == 205 then -- right
				selected = selected + 1
				if selected > #options then
					selected = 1
				end
			elseif event[2] == 28 then -- enter
				return selected , options[selected]
			end
		elseif event[1] == "mouse_click" then
			term.setCursorPos(1,1)
			term.clearLine()
			
			if bugTest then term.write("M "..event[2].." X "..event[3].." Y "..event[4]) end
			
			if event[2] == 1 then
				if event[4] == ofsY + 3 then
					for i = 1,#options do
						if event[3] >= range[i].s + ofC + ofsX - 1 and event[3] <= range[i].f + ofC + ofsX then
							return i , options[i]
						end
					end
				end
			end
		end
	end
end

local function rClickMenu(title,tList,posX,posY)

	term.setCursorBlink(false)
	local BoxTitle = title
	local choices = tList
	local termX,termY = term.getSize()
	local offX,offY
	
	local width = #BoxTitle + 2
	local hight = #choices + 1
	
	for i = 1,#choices do
		if width < #choices[i] + 2 then
			width = #choices[i] + 2
		end
	end
	
	offX,offY = math.ceil((termX/2) - (width/2)),math.ceil((termY/2) - (hight/2))
	
	if posX and posY then -- offX,offY = posX,posY
		if posX >= termX - width - 1 then
			offX = termX - width - 1
		else
			offX = posX
		end
		if posY >= termY - hight then
			offY = termY - hight
		else
			offY = posY
		end
	end
	
	local function reDrawer()
		printC(offX,offY,colors.black,colors.blue," "..BoxTitle..string.rep(" ",width - #BoxTitle - 1))
		for i = 1,#choices do
			printC(offX,offY + i,colors.black,colors.white," "..choices[i]..string.rep(" ",width - #choices[i] - 1))
		end
	end
	
	while true do
		reDrawer()
		local event = {os.pullEvent()}
		if event[1] == "mouse_click" then
			if event[2] == 1 then -- event[3] = x event[4] = y
				if event[4] > offY and event[4] < hight + offY and event[3] >= offX and event[3] < width + offX then
					return choices[event[4] - offY]
				else
					return
				end
			elseif event[2] == 2 then
				return
			end
		end
	end
	
end

local function osRunSpaces(...)
	clear()
	return os.run(getfenv(),...)
end

local function fileSelect(mode) -- save_file open_file browse < not yet implemented
	
	local title = "File Browser "..ver
	local bRun = true
	local flag = true
	local clipboard = nil
	local cut = false
	
	local termX,termY = term.getSize()
	local offsetX,offsetY = 1,1
	local hight,width = math.ceil(termY-2),math.ceil(termX-2)
	local oldHight,oldWidth
	
	local boxOffX,boxOffY = offsetX,offsetY + 2
	local boxH,boxW = hight - 2 ,width - 2
	
	local barX,barY = offsetX + 1,offsetY + 2
	local barH,barW = 1,width - 1
	
	local tbarX,tbarY = offsetX + 1,offsetY + 1
	local tbarH,tbarW = 1,width - 1
	
	local exitX,exitY = offsetX + width - 1 ,offsetY + 1
	
	local parth = {dir:match("[^/]+")}
	local list
	
	local fSlash = "/"
	local listOff = 0
	
	local function stringParth() -- compacted this alot
		return fSlash..table.concat(parth,fSlash)
	end
	
	local sParth = stringParth()
	local files = {}
	local folders = {}
	local disks = {}

	local function NewList()
		flag = true
		listOff = 0
		sParth = stringParth()
		files = {}
		folders = {}
		disks = {}
		test,list = pcall(fs.list,sParth)
		if list == nil then
			list = {}
			dialogBox("ERROR : ","fs.list crashed",{"ok"})
		end
		for i = 1,#list do
			if showAll or list[i]:sub(1,1) ~= "." then
				if fs.isDir(sParth..fSlash..list[i]) then
					table.insert(folders,list[i])
				else
					table.insert(files,list[i])
				end
			end
		end
		if #parth == 0 then
			for i,v in pairs(rs.getSides()) do
				if disk.isPresent(v) and disk.hasData(v) then
					disks[disk.getMountPath(v)] = v
				end
			end
		end
		table.sort(disks)
		table.sort(folders)
		table.sort(files)
	end
	
	NewList()
	
	local function size(test)
		if #test > boxW - 3 then
			return string.sub(test,1,boxW-3)
		end
		return test
	end
	
	
	while bRun do
		if flag then
			flag = false
			-- clear
			if oldHight ~= hight and oldWidth ~= width then
				term.setBackgroundColor(colors.black)
				term.clear()
				oldHight,oldWidth = hight,width
			end
			-- draw top title bar
			term.setCursorPos(tbarX,tbarY)
			term.setTextColor(colors.black)
			term.setBackgroundColor(colors.blue)
			local b = tbarW - #title -2
			if b < 0 then
				b = 0
			end
			term.write(string.sub(" "..title,1,tbarW)..string.rep(" ",b))
			term.setTextColor(colors.white)
			term.setBackgroundColor(colors.red)
			term.write("X")
			
			-- draw location bar
			
			term.setCursorPos(barX,barY)
			term.setTextColor(colors.black)
			term.setBackgroundColor(colors.lightGray)
			local a = barW - #sParth - 1
			if a < 0 then
				a = 0
			end
			local tmppath = sParth
			if shell and shell.getDisplayName then
				tmppath = shell.getDisplayName(sParth)
				--dialogBox("yay")
			else
				--dialogBox("moop")
			end
			tmppath = tmppath or sParth
			local a = barW - #tmppath - 1
			if a < 0 then
				a = 0
			end
			term.write(string.sub(" "..tmppath,1,barW)..string.rep(" ",a))
			
			-- draw scroll bar
			if folders[1] ~= ".." then 
			  table.insert(folders, 1, "..")
			end
			
			if #folders + #files > boxH then
				term.setBackgroundColor(colors.lightGray)
				for i = 1,boxH do
					term.setCursorPos(boxOffX+boxW+1,i + boxOffY)
					local scroll = math.floor( boxH* (listOff/(#folders + #files-boxH+2)) )+1
					if i == scroll then
						term.setBackgroundColor(colors.gray)
						term.write(" ")
						term.setBackgroundColor(colors.lightGray)
					else
						term.write(" ")
					end
				end
			else
				term.setBackgroundColor(colors.gray)
				for i = 1,boxH do
					term.setCursorPos(boxOffX+boxW+1,i + boxOffY)
					term.write(" ")
				end
			end
			
			-- draw main section
			
			term.setTextColor(colors.black)
			term.setBackgroundColor(colors.cyan)
			for i = 1,boxH do
				term.setCursorPos(1+boxOffX,i+boxOffY)
				if folders[i+listOff] then
					if disks[folders[i+listOff]] then
						term.setTextColor(colors.orange)
						term.setBackgroundColor(colors.blue)
						term.write("D!")
					else
						term.setTextColor(colors.blue)
						term.setBackgroundColor(colors.lightBlue)
						term.write("[]")
					end
					if folders[i+listOff]:sub(1,1) == "." and folders[i+listOff] ~= ".." then
						term.setTextColor(colors.gray)
					else
						term.setTextColor(colors.black)
					end
					term.setBackgroundColor(colors.cyan)
					local repTimes = boxW - #folders[i+listOff] - 3
					if repTimes < 0 then
						repTimes = 0
					end
					term.write(" "..size(folders[i+listOff])..string.rep(" ",repTimes))
				elseif files[i-#folders+listOff] then
					local repTimes = boxW - #files[i-#folders+listOff] - 3
					if repTimes < 0 then
						repTimes = 0
					end
					term.write("   "..size(files[i-#folders+listOff])..string.rep(" ",repTimes))
				else
					term.write(string.rep(" ",boxW))
				end
			end
			term.setTextColor(colors.white)
			term.setBackgroundColor(colors.black)
			
			if bugTest then
				term.setCursorPos(1,1)
				term.write(listOff.." "..boxOffY.." "..boxH)
			end
			
			-- resume("redraw")
		end
		-- react to events
		
		local event = {os.pullEvent()}
		
		if event[1] == "mouse_click" then
			if event[2] == 1 then -- left mouse
				local temp = nil
				if event[4] > boxOffY and event[4] <= boxH + boxOffY then
					temp = folders[event[4]+listOff-boxOffY] or files[event[4]-#folders+listOff-boxOffY]
				end
				if temp and event[3] > boxOffX and event[3] <= #temp + 3 + boxOffX and event[3] < boxOffX + boxW then
					if temp == ".." then
						table.remove(parth,#parth)
						NewList()
					elseif fs.isDir(stringParth()..fSlash..temp) then
						table.insert(parth,temp)
						NewList()
					else
						if fs.exists(stringParth()..fSlash..temp) then
							if dialogBox("Run file ?",temp,{"yes","no"}) == 1 then
								local test,info = osRunSpaces(stringParth()..fSlash..temp)
								term.setCursorBlink(false)
								if not test then
									dialogBox("ERROR",tostring(info),{"ok"})
								end
							end
							flag = true
						end
					end
				elseif event[3] == boxOffX+boxW+1 and event[4] > boxOffY and event[4] <= boxOffY+boxH then
					if #folders + #files > boxH then
						if event[4] == boxOffY + 1 then
							listOff = 0
						elseif event[4] == boxOffY+boxH then
							listOff = #folders + #files + 1 - boxH
						else
							listOff = math.ceil((event[4] - boxOffY - 1 )*(((#folders + #files - boxH+2)/boxH)))
						end
						flag = true
					end
				elseif event[3] == exitX and event[4] == exitY then
					if dialogBox("Confirm","Exit application",{"yes","no"}) == 1 then
						bRun = false
					end
					flag = true
				end
			elseif event[2] == 2 then -- right mouse
				local temp = nil
				local sChoice = nil
				local sType = nil
				
				if event[4] > boxOffY and event[4] <= boxH + boxOffY then
					temp = folders[event[4]+listOff-boxOffY] or files[event[4]-#folders+listOff-boxOffY]
				end
				
				-- moved
				local paste
				if clipboard then
					paste = "Paste"
				end
				
				if temp and event[3] > boxOffX and event[3] <= #temp + 3 + boxOffX and event[3] < boxOffX + boxW then
					sType = "File"
					
					if fs.isDir(stringParth()..fSlash..temp) then
						sType = "Folder"
					end
					
					if disks[temp] then
						sChoice = rClickMenu("Options",{"Open","Eject"},event[3]+1,event[4]+1)
					else
						if sType == "Folder" then
							sChoice = rClickMenu("Options",{"Open","Delete"},event[3]+1,event[4]+1)
						else
							if windowDimensions then
								sChoice = rClickMenu("Options",{"Run","Run CMD","deBug","Run win","Rename","Edit","Paint","Delete", "Cut", "Copy", paste},event[3]+1,event[4]+1)
							else
								sChoice = rClickMenu("Options",{"Run","Run CMD","deBug","CHIP 8","Rename","Edit","Paint","Delete", "Cut", "Copy", paste},event[3]+1,event[4]+1)
							end
						end
					end
					
				else
					if event[3] > boxOffX and event[3] <= 5 + boxOffX and event[3] < boxOffX + boxW then
						if event[4] > offsetY and event[4] < hight + offsetY then
							sChoice = rClickMenu("Options",{"New File","New Folder", paste},event[3]+1,event[4]+1)
						end
					else
						table.remove(parth,#parth)
						NewList()
					end
				end
				
				if sChoice == "Run win" and windowDimensions then -- choice execution
					osRunSpaces("start",stringParth()..fSlash..temp) -- might be a probblem -- shell
					flag = true
				elseif sChoice == "deBug" then
					osRunSpaces(stringParth()..fSlash..temp)
					term.setCursorBlink(false)
					os.pullEvent("key")
				elseif sChoice == "CHIP 8" then -- this was a temo hackin will be going
					local function openDevice(sType)
						for i,v in pairs(rs.getSides()) do
							if peripheral.isPresent(v) and peripheral.getType(v) == sType then
								return peripheral.wrap(v),v
							end
						end
					end
					local func,side = openDevice("monitor")
					osRunSpaces("chip8",stringParth()..fSlash..temp,side)
				elseif sChoice == "New File" then -- experemntal
					local name = InputBox()
					if name then
						if fs.exists(stringParth()..fSlash..name) then
							dialogBox("ERROR","Name exists",{"ok"})
						else
							local file = fs.open(stringParth()..fSlash..name,"w")
							if file then
								file.write("")
								file.close()
								NewList()
							else
								dialogBox("ERROR","File not created",{"ok"})
							end
						end
					end
				elseif sChoice == "New Folder" then -- experemental
					local name = InputBox()
					if name then
						if fs.exists(stringParth()..fSlash..name) then
							dialogBox("ERROR","Name exists",{"ok"})
						else
							if pcall(fs.makeDir,stringParth()..fSlash..name) then
								NewList()
							else
								dialogBox("ERROR","Access Denied",{"ok"})
							end
						end
					end
				elseif sChoice == "Open" then
					table.insert(parth,temp)
					NewList()
				elseif sChoice == "Run" then
					local test,info = osRunSpaces(stringParth()..fSlash..temp)
					term.setCursorBlink(false)
					if not test then
						dialogBox("ERROR",tostring(info),{"ok"})
					end
				elseif sChoice == "Run CMD" then
					local cmd = fixArgs(InputBox("Commands"))
					if cmd ~= nil then
						local test,info = osRunSpaces(stringParth()..fSlash..temp,unpack(cmd))
						term.setCursorBlink(false)
						if not test then
							dialogBox("ERROR",tostring(info),{"ok"})
						end
					end
				elseif sChoice == "Edit" then
					if sType == "File" then
						osRunSpaces("/rom/programs/edit",stringParth()..fSlash..temp)
					else
						dialogBox("ERROR","Can't edit a Folder",{"ok"})
					end
				elseif sChoice == "Delete" then
					if dialogBox("Confirm","Delete "..sType.." "..temp,{"yes","no"}) == 1 then
						if fs.isReadOnly(stringParth()..fSlash..temp) then
							dialogBox("ERROR",sType.." Is read Only",{"ok"})
						else
							fs.delete(stringParth()..fSlash..temp)
							NewList()
						end
					end
				elseif sChoice == "Paint" then
					if sType == "File" then
						osRunSpaces("/rom/programs/color/paint",stringParth()..fSlash..temp)
					else
						dialogBox("ERROR","Can't edit a Folder",{"ok"})
					end
				elseif sChoice == "Eject" then
					if dialogBox("Confirm","Eject disk "..disks[temp].." "..fSlash..temp,{"yes","no"}) == 1 then
						disk.eject(disks[temp])
						NewList()
					end
				elseif sChoice == "Copy" then
					clipboard = {stringParth(), temp}
					cut = false
				elseif sChoice == "Cut" then
					clipboard = {stringParth(), temp}
					cut = true
				elseif sChoice == "Paste" then
					if cut then
						local s, m = pcall(function()
							fs.move(clipboard, stringParth().."/"..temp)
							cut = false
							clipboard = nil
						end)
						if not s then
							dialogBox("Error", (m or "Couldn't move"), {"ok"}, 4, 30)
						end
						if bugTest then
							local x, y = term.getCursorPos()
							term.setCursorPos(1, ({term.getSize()})[2])
							write("from "..clipboard[1].."/"..clipboard[2].." to "..stringParth().."/"..clipboard[2])
						end
					else
						local s, m = pcall(function()
							fs.copy(clipboard[1].."/"..clipboard[2], stringParth().."/"..clipboard[2])
						end)
						if not s then
							dialogBox("Error", (m or "Couldn't copy"), {"ok"}, 4, 30)
						end
						if bugTest then
							local x, y = term.getCursorPos()
							term.setCursorPos(1, ({term.getSize()})[2])
							write("from "..clipboard[1].."/"..clipboard[2].." to "..stringParth().."/"..clipboard[2])
						end
					end
					NewList()
				elseif sChoice == "Rename" then -- new parts of the above thanks kilo
					local sName = InputBox("New Name")
					if type(sName) == "string" and sName ~= "" then
						local s, m = pcall(function()
							fs.move(stringParth()..fSlash..temp,stringParth()..fSlash..sName)
						end)
						if not s then
							dialogBox("Error", (m or "Rename failed"), {"ok"})
						end
					end
					NewList()
				end
				flag = true
			end
		elseif event[1] == "mouse_scroll" then -- flag this needs new math
			local old = listOff
			listOff = listOff + event[2]
			if listOff < 0 then
				listOff = 0
			end
			if #folders + #files + 1 - boxH > 0 and listOff > #folders + #files + 1 - boxH then
				listOff = #folders + #files + 1 - boxH
			elseif listOff > 0 and #folders + #files + 1 - boxH < 0 then
				listOff = 0
			end
			if listOff ~= old then
				flag = true
			end
		
		elseif event[1] == "mouse_drag" then -- test
			if event[3] == boxOffX+boxW+1 and event[4] > boxOffY and event[4] <= boxOffY+boxH then
				if #folders + #files > boxH then
					if event[4] == boxOffY + 1 then
						listOff = 0
					elseif event[4] == boxOffY+boxH then
						listOff = #folders + #files + 1 - boxH
					else
						listOff = math.ceil((event[4] - boxOffY - 1 )*(((#folders + #files - boxH+2)/boxH)))
					end
					flag = true
				end
			end
			
		elseif event[1] == "disk" or event[1] == "disk_eject" then
			NewList()
		elseif event[1] == "window_resize" then
			termX,termY = term.getSize()
			offsetX,offsetY = 1,1
			hight,width = math.ceil(termY-2),math.ceil(termX-2)
			
			boxOffX,boxOffY = offsetX,offsetY + 2
			boxH,boxW = hight - 2 ,width - 2
			
			barX,barY = offsetX + 1,offsetY + 2
			barH,barW = 1,width - 1
			
			tbarX,tbarY = offsetX + 1,offsetY + 1
			tbarH,tbarW = 1,width - 1
			
			exitX,exitY = offsetX + width - 1 ,offsetY + 1
			
			flag = true
		end
	end
end
local function main()
	if term.isColor() then
		clear()
		fileSelect()
		clear()
	else
		error("Not an Advanced Computer (gold) ")
	end
end
local trash = (norun or main())