local tArgs = {...}
local apiurl = "https://api.bitbucket.org/1.0/repositories/Kilobyte/kilos/"
local cfg = config.load("/etc/devupdate.conf") or {}
local updatedir = ""


for a = 1, #tArgs do
  if tArgs[a]:sub(1,2) == "--" then
    local cmd = tArgs[a]:sub(3):lower()
    if cmd == "source" then
      apiurl = tArgs[a+1]
      if apiurl:sub(-1, -1) ~= "/" then
        apiurl = apiurl.."/"
      end
      a = a + 1
    end
  elseif tArgs[a]:sub(1,1) == "-" then
    for b = 2, #tArgs[a] do
      cmd = tArgs[a]:sub(b, b)
      if cmd == "s" then
        
      end
    end
  else
    table.insert(tArgs_, tArgs[a])
  end
end


local function crawlFiles(path)
  local h = http.get(apiurl.."src/master"..path)
  local t = h.readAll()
  local dir = json.decode(t)
  for a = 1, #dir.files do
    local name = dir.files[a].path
    print("Downloading /"..name)
    local h2 = http.get(apiurl.."raw/master/"..name)
    local f = fs.open(updatedir..name, "w")
    local c = h2.readAll() 
    f.write(c)
    f.close()
    h.close()
  end
  for a = 1, #dir.directories do
    local name = dir.directories[a]
    if not fs.exists(updatedir..path..name) then fs.makeDir(updatedir..path..name) end
    crawlFiles(path..name.."/")
  end
end

local function getCommits()
  local h = http.get(apiurl.."changesets?limit=5")
  local t = h.readAll()
  local changesets = json.decode(t).changesets
  --[[local commits = {}
  for a = 1, #events do
    local e = events[a]
    if e.event == "commit" then
      table.insert(commits, e)
    end
  end--]]
  return changesets
end

local function main()
  --if not fs.exists("/devupdate") then fs.makeDir("/devupdate") end
  local c = getCommits()
  print("Found "..#c.." commits")
  if c[#c].raw_node ~= cfg.node then
    --update required
    local pe = os.pullEvent
    os.pullEvent = os.pullEventRaw
    print("This may take a while. please don't shut down the computer")
    crawlFiles("/")
    cfg.node = c[#c].node
    config.save("/etc/devupdate.conf", cfg)
    os.pullEvent = pe
    print("press any key to reboot")
    os.pullEventRaw("key")
    os.reboot()
  else
    --up-to-date
    print("Everything is up-to-date")
  end
end
main()