local function loadImpl(tLines, pos)
  local ret = {}
  while tLines[pos] do
    local l = tLines[pos]
    pos = pos + 1
    if string.sub(l, 1, 1) == "#" then
      --the line is a comment
    elseif string.find(l, "=") then
      local eqpos = string.find(l, "=")
      local key, val
      key = string.sub(l, 1, eqpos-1)
      val = string.sub(l, eqpos + 3)
      sType = string.sub(l, eqpos + 1, eqpos + 1)
      if sType == "s" then
        ret[key] = val
      elseif sType == "n" then
        ret[key] = tonumber(val)
      elseif sType == "b" then
        ret[key] = (val == "true")
      end
    elseif string.sub(l, -2, -1) == "[[" then
      ret[string.sub(l, 1, -3)], pos = loadImpl(tLines, pos)
    elseif l == "]]" then
      break
    elseif l == "" then
    else
      error("Invalid Config File")
    end
  end
  return ret, pos
end
 
local function readAsTable(_sFile, _fs)
  local f = _fs.open(_sFile, "r")
  local ret = {}
  while true do
    local line = f.readLine()
    if not line then break end
    table.insert(ret, line)
  end
  f.close()
  return ret
end
--[[
    Loads a config file
   
    @param _sFile The name of the file where the config should be loaded from.
    Optional @param _fs a file system to use. leave empty or set to nil if you don't know what this means
   
    @returns a table containing the config info
--]]
function load(_sFile, _fs)
  if not _fs then _fs = fs end
  if not _fs.exists(_sFile) and not _fs.isReadOnly(_sFile) then local f = _fs.open(_sFile, "w") print(f) f.close() end
  if not _fs.isDir(_sFile) then
    local lines = readAsTable(_sFile, _fs)
    local ret, pos = loadImpl(lines, 1)
    return ret
  end
  print("Found you!")
  return nil, "couldn't load config"
end
 
local function saveImpl(tIn)
  local ret = ""
  for i, v in pairs(tIn) do
    if string.find(i, "=") then
      error("config option key must not contain '='")
    end
    if string.sub(i, 1, 1) == "#" then
      error("key must not start with '#'")
    end
    sType = type(v)
    if sType == "string" then
      ret = ret..i.."=s:"..v.."\n"
    elseif sType == "number" then
      ret = ret..i.."=n:"..v.."\n"
    elseif sType == "boolean" then
      local v_ = "false" if v then v_ = "true" end
      ret = ret..i.."=b:"..v_.."\n"
    elseif sType == "table" then
      ret = ret..i.."[[\n"
      ret = ret..saveImpl(v).."]]\n"
    end
  end
  return ret
end
 
--[[
    Saves config options from a table to a file
   
    @param _sFile The filename/path
    @param _tCfg The config option table to write
    @_tComments no effect yet, set to nil or leave out
    Optional @param _fs The filesystem to use. see above
--]]
function save(_sFile, _tCfg, _tComments, _fs)
  if not _fs then _fs = fs end
  local data = saveImpl(_tCfg)
  local f = _fs.open(_sFile, "w")
  f.write(data)
  f.close()
end