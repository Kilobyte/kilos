--[[ Variables and Tables ]]--

local bColor = term.isColor()
local tManDirs = {
	"/man/",
	"/usr/man/"
}
local tColor = {
	["0"] = 1,		-- white
	["1"] = 2,		-- orange
	["2"] = 4,		-- magenta
	["3"] = 8,		-- light blue
	["4"] = 16,		-- yellow
	["5"] = 32,		-- lime
	["6"] = 64,		-- pink
	["7"] = 128,	-- gray
	["8"] = 256,	-- light gray
	["9"] = 512,	-- cyan
	["a"] = 1024,	-- purple
	["b"] = 2048,	-- blue
	["c"] = 4096,	-- brown
	["d"] = 8192,	-- green
	["e"] = 16384,	-- red
	["f"] = 32768	-- black
}


--[[  Local Functions  ]]--

local function clear()
	term.clear()
	term.setCursorPos(1,1)
end

local function appendTable(tableOne, tableTwo) 
	while true do
		local entry = table.remove(tableTwo, 1)
		if not entry then
			return tableOne
		else
			table.insert(tableOne, entry)
		end 
	end
end

local function split(sLine)
	local x,y = term.getSize()
	local tLines = {}
	local i = 1
	if #sLine > 0 and sLine:find("[^ ]") then
		for str in sLine:gmatch("(%s*[^ ]+)") do
			local sAppendedLine = tLines[i] and tLines[i]..str or str
			if sAppendedLine:gsub("§[%x]",""):len() <= x then
				tLines[i] = sAppendedLine
			else
				i = i + 1
				if str:find("^ ") then str = str:gsub(" ","",1) end
				tLines[i] = str
			end
		end
	else tLines[1] = sLine end
	return tLines
end

local function getFilePath(sName,sLang)
	local sPath
	for a=1, #tManDirs do
		if fs.exists(tManDirs[a]..sLang) then
			local tList = fs.list(tManDirs[a]..sLang)
			for b=1, #tList do
				if tList[b] == sName then sPath = tManDirs[a]..sLang.."/"..sName break end
			end
			if sPath then return sPath end
		end
	end
	return ""
end

local function load(sName)
	local lang = user.getLang()
	local tLines = {}
	local tRetLines = {}
	local sPath = ""
	sPath = getFilePath(sName,lang)
	if sPath:len() == 0 then sPath = getFilePath(sName,"en-us") end
	if sPath:len() == 0 then return end
	if fs.exists(sPath) and not fs.isDir(sPath) then
		local file = io.open( sPath, "r" )
		local sLine = file:read()
		while sLine do
			table.insert( tLines, sLine )
			sLine = file:read()
		end
		file:close()
		for i=1,#tLines do
			tRetLines = appendTable(tRetLines,split(tLines[i]))
		end
		for i=2,#tRetLines do
			if not tRetLines[i]:find("^(§[%x])") then
				local modifier
				for a = i-1,1,-1 do
					for str in tRetLines[a]:gmatch('(§[%x])') do modifier = str end
					if modifier then tRetLines[i] = modifier..tRetLines[i] break end
				end
			end
		end		
		return tRetLines
	else return false end
end

local function writeLine(sLine)
	local x,y = term.getSize()
	local nCurrentPos = 1
	local nColorModifierPos
	local tParts = {}
	if not sLine:find("^(§[%x])") then sLine = "§0"..sLine end
	for str in sLine:gmatch("(§[%x][^§]+)") do table.insert(tParts,str) end
	for i=1, #tParts do
		local sColor,sPart = tParts[i]:match("§([%x])(.+)")
		if term.isColor() and tColor[sColor] then term.setTextColor(tColor[sColor]) end
		term.write(sPart)
	end
end

local function redrawText(tLines,nScroll)
	local x,y = term.getSize()
	for i=1,y do
		term.setCursorPos( 1, i)
		term.clearLine()
		local sLine = tLines[ i + nScroll ]
		if sLine ~= nil then
			writeLine(sLine)
		end
	end
end
--[[  API Functions  ]]--

-- Print a man page
function open(sName)
	local x,y = term.getSize()
	local tLines = load(sName)
	local nScroll = 0
	if tLines then
		redrawText(tLines,nScroll)
		while true do
			local event, p1, p2, p3 = os.pullEvent()
			if event == "key" then
				if p1 == keys.q then
					clear()
					sleep(0)
					break
				elseif p1 == keys.up then
					if nScroll > 0 then
						nScroll = nScroll - 1
						redrawText(tLines,nScroll)
					end
				elseif p1 == keys.down then
					local nMaxScroll = #tLines - (y)
					if nScroll < nMaxScroll then
						nScroll = nScroll + 1
						redrawText(tLines,nScroll)
					end
				elseif p1 == keys.w or p1 == keys.pageUp then
					if nScroll - y >= 0 then
						nScroll = nScroll - y
						redrawText(tLines,nScroll)
					elseif nScroll > 0 then
						nScroll = 0
						redrawText(tLines,nScroll)
					end
				elseif p1 == keys.space or p1 == keys.pageDown then
					local nMaxScroll = #tLines - (y)
					if nScroll + y <= nMaxScroll then
						nScroll = nScroll + y
						redrawText(tLines,nScroll)
					elseif nScroll < nMaxScroll then
						nScroll = nMaxScroll
						redrawText(tLines,nScroll)
					end
				end
			elseif event == "mouse_scroll" then
				if p1 == -1 then
					if nScroll > 0 then
						nScroll = nScroll - 1
						redrawText(tLines,nScroll)
					end			
				elseif p1 == 1 then
					local nMaxScroll = #tLines - (y)
					if nScroll < nMaxScroll then
						nScroll = nScroll + 1
						redrawText(tLines,nScroll)
					end			
				end
			end
		end
	else 
		term.setTextColor(colors.red)
		write("The manual page '"..sName.."' doesn't exist.\n") 
	end
end